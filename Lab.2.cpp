#include <iostream>

using namespace std;

constexpr auto separator1 = "\xCD\xCD\xCD\xCD\xCD";
constexpr auto separator2 = "\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD";

double rootDegree(double, double, int, int&);

int main()
{
	double beginSegment;
	double endSegment;
	double step;
	double epsilon;
	int degree;
	cout << " Enter a,b,h (type real), eps (epsilon) and degree \n";
	cin >> beginSegment >> endSegment >> step >> epsilon >> degree;
	system("cls");
	cout << "\n  Table of function's values the root of " << degree << "- power from x \n\n" ;
	cout << " \xC9" << separator1 << "\xCB" << separator2 << "\xCB" << separator2 << "\xBB\n";
	cout << " \xBA  x  \xBA   degree  \xBA    pow    \xBa\n";
	cout << " \xCC" << separator1 << "\xCE" << separator2 << "\xCE" << separator2 << "\xB9\n";
	int numIteration = 0;
	for (double x = beginSegment; x < endSegment + step; x += step)
	{
		int& iteration = numIteration;
		double y = rootDegree(x, epsilon, degree, iteration);
		if (iteration > numIteration) numIteration = iteration;
		cout << " \xBA ";
		std::cout.setf(std::ios::fixed, std::ios::floatfield);
		cout.precision(1);
		cout << x;
		cout << " \xBA ";
		cout.precision(7);
		cout << y;
		cout << " \xBA ";
		cout.precision(7);
		cout << pow(x, 1. / degree);
		cout << " \xBA\n";
	}
	cout << " \xC8" << separator1 << "\xCA" << separator2 << "\xCA" << separator2 << "\xBC\n";
	cout << "  The maximum numder of iteration is equal to " << numIteration << "\n";
	system("pause");
	return 0;
}

double rootDegree(double x, double eps, int k, int& iteration)
{
	double previos = x;
	double following = ((k - 1) * previos + x / pow(previos, k)) / k;
	iteration = 0;
	while (fabs(previos - following) > eps)
	{
		previos = following;
		following = ((k - 1) * previos + x / pow(previos, k - 1)) / k;
		iteration++;
	}
	return following;
}
